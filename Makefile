BIN_NAME = ts3bot

CXX_COMPILER = "g++"

SOURCES = 	Source/main.cpp \
		Source/userbase.cpp \
		Source/teamspeak.cpp \
		Source/utilities.cpp

INCLUDE = -ILibraries/asio-1.10.6/include -IHeader
LINK = -lpthread -lmysqlcppconn

OBJECTS_CPP = $(patsubst %.cpp,%.o,$(SOURCES))

CXX_FLAGS = -O3 -std=c++11

all: $(BIN_NAME)

%.o: %.cpp
	$(CXX_COMPILER) $(CXX_FLAGS) $(INCLUDE) -c -o $@ $<

$(BIN_NAME): $(OBJECTS_CPP)
	$(CXX_COMPILER) -o $@ $(OBJECTS_CPP) $(LINK)

clean:
	rm -f $(OBJECTS_CPP) $(BIN_NAME)

.PHONY: all clean
