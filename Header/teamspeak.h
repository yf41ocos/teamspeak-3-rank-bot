#pragma once

#include <iostream>

#include <string>
#include <utility>

#include <asio.hpp>

#include "utilities.h"

class teamspeak {

private:

  const std::string ts_host;
	const std::string ts_port;
	const std::string ts_user;
	const std::string ts_pwd;

  const std::string ts_ok_msg = "error id=0 msg=ok\n";

  std::string read(asio::ip::tcp::socket &socket) const;
  std::string read_until(asio::ip::tcp::socket &socket,const std::string &until,const std::string &error="") const;
  void post(const std::string &data, asio::ip::tcp::socket &socket) const;

  asio::ip::tcp::socket connect() const;
  std::string truncate_ts_uid(const std::string &uid) const;


public:

  teamspeak(
    const std::string &ts_host,
  	const std::string &ts_port,
  	const std::string &ts_user,
  	const std::string &ts_pwd
  );

  void get_ids(std::vector<std::string> &res) const;
  void upgrade_memberships(
    std::map<std::string,std::pair<bool,unsigned long long>> &ts_users,
  	std::map<std::string,size_t> &forum_users
   ) const;
};
