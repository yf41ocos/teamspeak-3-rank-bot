#pragma once

#include <string>
#include <vector>
#include <map>
#include <utility>

#include "teamspeak.h"

class userbase {

private:

	const std::string relay_host;
	const std::string relay_user;
	const std::string relay_pwd;

	//connection details for local database
	const std::string mysql_host;
	const std::string mysql_port;
	const std::string mysql_user;
	const std::string mysql_pwd;
	const std::string mysql_db;

	//connection details for remote database
	const std::string remote_mysql_host;
	const std::string remote_mysql_port;
	const std::string remote_mysql_user;
	const std::string remote_mysql_pwd;
	const std::string remote_mysql_db;

	//rescan teamspeak every 10 seconds
	const unsigned long long ts_tickrate;

	//every known ts user ever, bool means is online right now
	std::map<std::string,std::pair<bool,unsigned long long>> ts_users;

	//every user from the forum db with the ts uid
	std::map<std::string,size_t> forum_users;

public:

	userbase(
		const std::string &relay_host,
		const std::string &relay_user,
		const std::string &relay_pwd,
		const std::string &mysql_host,
		const std::string &mysql_port,
		const std::string &mysql_user,
		const std::string &mysql_pwd,
		const std::string &mysql_db,
		const std::string &remote_mysql_host,
		const std::string &remote_mysql_port,
		const std::string &remote_mysql_user,
		const std::string &remote_mysql_pwd,
		const std::string &remote_mysql_db,
		const unsigned long long tickrate
	);

	void rescan_teamspeak(
		const std::string &ts_host,
		const std::string &ts_port,
		const std::string &ts_user,
		const std::string &ts_pwd
	);

	void reload_remote_sql_database();

	void save_user_online_times_to_sql_database();

	void apply_rules(
		const std::string &ts_host,
		const std::string &ts_port,
		const std::string &ts_user,
		const std::string &ts_pwd
	);

};
