#ifndef _UTILITIES_HEADER
#define _UTILITIES_HEADER

#include <string>
#include <vector>

extern void split(const std::string &s, char delim, std::vector<std::string> &elems);
extern std::vector<std::string> split(const std::string &s, char delim);

#endif
