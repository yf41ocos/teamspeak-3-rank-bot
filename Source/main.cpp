#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <asio.hpp>
#include <chrono>
#include <thread>

#include "mysql_driver.h"
#include <cppconn/prepared_statement.h>

#include "utilities.h"
#include "userbase.h"

int main(int argc, char **argv)
{

	std::cout << "ts3 bot initializing" << std::endl;

#ifdef ASIO_STANDALONE
	std::cout << "run asio standalone" << std::endl;
#endif

#ifdef BOOST_DATE_TIME_NO_LIB
	std::cout << "disabled boost date" << std::endl;
#endif

#ifdef BOOST_REGEX_NO_LIB
	std::cout << "disabled boost regex" << std::endl;
#endif

	//load config file
	std::ifstream config;
	config.open("ts3bot.config");
	std::string line;

	std::vector<std::string> ts_data;
	std::vector<std::string> local_mysql_data;
	std::vector<std::string> remote_mysql_data;
	std::vector<std::string> relay_data;

	auto *scope_data = &ts_data;

	while ( getline (config,line) )
	{
	 if(line == "[ts3]") scope_data = &ts_data;
	 else if(line == "[local_mysql]") scope_data = &local_mysql_data;
	 else if(line == "[remote_mysql]") scope_data = &remote_mysql_data;
	 else if(line == "[relay]") scope_data = &relay_data;
	 else scope_data->push_back(line);
	}
  config.close();

	std::string ts_host = split(ts_data[0],' ')[1];
	std::string ts_port = split(ts_data[1],' ')[1];
	std::string ts_user = split(ts_data[2],' ')[1];
	std::string ts_pwd = split(ts_data[3],' ')[1];

	std::string local_mysql_host = split(local_mysql_data[0],' ')[1];
	std::string local_mysql_port = split(local_mysql_data[1],' ')[1];
	std::string local_mysql_user = split(local_mysql_data[2],' ')[1];
	std::string local_mysql_pwd = split(local_mysql_data[3],' ')[1];
	std::string local_mysql_db = split(local_mysql_data[4],' ')[1];

	std::string remote_mysql_host = split(remote_mysql_data[0],' ')[1];
	std::string remote_mysql_port = split(remote_mysql_data[1],' ')[1];
	std::string remote_mysql_user = split(remote_mysql_data[2],' ')[1];
	std::string remote_mysql_pwd = split(remote_mysql_data[3],' ')[1];
	std::string remote_mysql_db = split(remote_mysql_data[4],' ')[1];

	std::string relay_host = split(relay_data[0],' ')[1];
	std::string relay_user = split(relay_data[1],' ')[1];
	std::string relay_pwd = split(relay_data[2],' ')[1];


	unsigned long long tickrate = 600;

	userbase usrbase(
		relay_host,
		relay_user,
		relay_pwd,
		local_mysql_host,
		local_mysql_port,
		local_mysql_user,
		local_mysql_pwd,
		local_mysql_db,
		remote_mysql_host,
		remote_mysql_port,
		remote_mysql_user,
		remote_mysql_pwd,
		remote_mysql_db,
		tickrate
	);

	std::cout << "initialization done" << std::endl;

	while(true){
		std::cout << "reload_remote_sql_database" << std::endl;
		usrbase.reload_remote_sql_database();
		std::cout << "rescan_teamspeak" << std::endl;
		usrbase.rescan_teamspeak(ts_host,ts_port,ts_user,ts_pwd);
		std::cout << "apply_rules" << std::endl;
		usrbase.apply_rules(ts_host,ts_port,ts_user,ts_pwd);
		std::cout << "save_user_online_times_to_sql_database" << std::endl;
		usrbase.save_user_online_times_to_sql_database();
		std::cout << "sleep" << std::endl;
		sleep(tickrate);
	}
	return 0;
}
