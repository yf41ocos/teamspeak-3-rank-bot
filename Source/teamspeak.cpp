#include "teamspeak.h"

teamspeak::teamspeak(
  const std::string &ts_host,
  const std::string &ts_port,
  const std::string &ts_user,
  const std::string &ts_pwd
) :
  ts_host(ts_host),
  ts_port(ts_port),
  ts_user(ts_user),
  ts_pwd(ts_pwd)
{}

asio::ip::tcp::socket teamspeak::connect() const
{
  //setup service
  asio::io_service io_service;
  asio::ip::tcp::socket socket(io_service);
  asio::ip::tcp::resolver resolver(io_service);

  try {
    asio::connect(socket, resolver.resolve({ts_host,ts_port}));
  }
  catch (asio::system_error &e) {
    std::cout << e.what() << std::endl;
    socket.close();
    return socket;
  }
  //initial welcome message
  read_until(socket,"for information on a specific command.");

  //login
  post("login "+ts_user+" "+ts_pwd, socket);
  read_until(socket,ts_ok_msg);

  return socket;
}

std::string teamspeak::read(asio::ip::tcp::socket &socket) const
{
	//wait till there is data, read it
	size_t availableBytes = 0;
	while (availableBytes == 0) {
		availableBytes = socket.available();
	}

	std::stringstream ss;
	size_t reply_length;
	char *data = new char[availableBytes+1];

	reply_length = asio::read(socket,
		asio::buffer(data, availableBytes));
	data[availableBytes] = '\0';

	for (size_t i = 0; i < reply_length; ++i) ss.put(data[i]);

	std::string result = ss.str();

	delete[] data;


#ifdef DEBUG
	std::cout << result << std::endl;
#endif

	return result;
}

namespace {

  class FoundMe {

  private:
    const std::string name;
    size_t ptr;

  public:
    FoundMe(const std::string &name) : name(name), ptr(0) {}

    bool next(const char &c)
    {
      if(ptr < name.size() && c == name.at(ptr)){
        ptr++;
        if(ptr == name.size()) return true;
      } else {
        ptr = 0;
      }
      return false;
    }

  };

}

std::string teamspeak::read_until(asio::ip::tcp::socket &socket,const std::string &until,const std::string &error) const
{
  std::stringstream msg;
  size_t availableBytes = 0;

  size_t until_ptr = 0;
  size_t error_until_ptr = 0;
  FoundMe until_finder(until);
  FoundMe error_finder(error);
  bool found_until = false;
  bool found_error = false;

  while(true){
    availableBytes = socket.available();
    if(availableBytes > 0){
      char *data = new char[availableBytes];
      asio::read(socket,asio::buffer(data, availableBytes));

      for (size_t i = 0; i < availableBytes; ++i) {
        char &c = data[i];
        if(until_finder.next(c)) found_until = true;
        if(error_finder.next(c)) found_error = true;
        msg.put(c);
      }

      delete[] data;
      if(found_until) break;
      if(found_error) return "error";
    }
  }

  std::string result = msg.str();

  #ifdef DEBUG
  	std::cout << result << std::endl;
  #endif

  return result;
}

void teamspeak::post(const std::string &data, asio::ip::tcp::socket &socket) const
{
#ifdef DEBUG
	std::cout << data << std::endl;
#endif
	asio::write(socket, asio::buffer(data+"\n"));
}

void teamspeak::get_ids(std::vector<std::string> &res) const
{
  asio::io_service io_service;
  asio::ip::tcp::socket socket(io_service);
  asio::ip::tcp::resolver resolver(io_service);

  try {
    asio::connect(socket, resolver.resolve({ts_host,ts_port}));
  }
  catch (asio::system_error &e) {
    std::cout << e.what() << std::endl;
    return;
  }
  //initial welcome message
  read_until(socket,"for information on a specific command.");

  //login
  post("login "+ts_user+" "+ts_pwd, socket);
  read_until(socket,ts_ok_msg);

  post("use sid=1 -virtual", socket);
  read_until(socket,ts_ok_msg);

  post("clientlist -uid",socket);
	std::string users = read_until(socket,ts_ok_msg);

	auto users_vector = split(users, '|');

  std::vector<std::string> excluded_ids = {"serveradmin"};

  auto valid = [&](const std::string &s)
  {
    for(auto &ex : excluded_ids){
      if(s.find(ex) != std::string::npos) {
        return false;
      }
    }
    return true;
  };


	for (auto &s : users_vector)
	{
  	auto parts = split(s,' ');
		for (auto &p : parts)
		{
			if (p.find("client_unique_identifier=") != std::string::npos)
			{
        std::string uid = p.substr(25, p.size()-1);
        if(valid(uid)) res.push_back(truncate_ts_uid(uid));
        break;
			}
		}
	}
  asio::error_code ec;
  socket.shutdown(asio::ip::tcp::socket::shutdown_both, ec);
  socket.close();
}

std::string teamspeak::truncate_ts_uid(const std::string &uid) const
{
  size_t i = 0;
  for(;i < uid.size();++i){
    if(uid.at(i) == '='){
      i++;
      break;
    }
  }
  return uid.substr(0,i);
}

void teamspeak::upgrade_memberships(
  std::map<std::string,std::pair<bool,unsigned long long>> &ts_users,
  std::map<std::string,size_t> &forum_users
 ) const
 {

   //get groups of every user


   asio::io_service io_service;
   asio::ip::tcp::socket socket(io_service);
   asio::ip::tcp::resolver resolver(io_service);

   try {
     asio::connect(socket, resolver.resolve({ts_host,ts_port}));
   }
   catch (asio::system_error &e) {
     std::cout << e.what() << std::endl;
     return;
   }
   //initial welcome message
   read_until(socket,"for information on a specific command.");

   //login
   post("login "+ts_user+" "+ts_pwd, socket);
   read_until(socket,ts_ok_msg);

   post("use sid=1 -virtual", socket);
   read_until(socket,ts_ok_msg);

   auto extract_sgid = []( const std::string &s ) -> std::string
   {
     auto parts = split(s,' ');
     for(auto &p : parts)
     {
       if(p.find("sgid=") != std::string::npos){
         return p.substr(5,p.size()-1);
       }
     }
     return "error";
   };

   for(auto &forum_user : forum_users)
   {
     std::string uid = forum_user.first;
     if(uid.size() == 0) continue;
     post("clientdbfind pattern="+uid+" -uid",socket);
     std::string result = read_until(socket,ts_ok_msg,"error id=1281 msg=database\\sempty\\sresult\\sset");
     if(result == "error") continue;
     std::string cldbid_pre = split(result,'\n')[0];
     std::string cldbid = cldbid_pre.substr(7,cldbid_pre.size()-1);

     auto it = ts_users.find(uid);

     if(it != ts_users.end()){

       //get group id
       post("servergroupsbyclientid cldbid="+cldbid,socket);
       auto res_groups = read_until(socket,ts_ok_msg);
       auto res_groups_trunc = split(res_groups,'\n')[0];
       auto groups = split(res_groups_trunc,'|');

       bool is_guest = true;
       for(auto &group : groups) {
         if(extract_sgid(group) != "8") is_guest = false;
       }
       //if group id is guest set to member
       if(is_guest) {
         post("servergroupaddclient sgid=7 cldbid="+cldbid,socket);
         read_until(socket,ts_ok_msg);
       }
     }

   }
 }
