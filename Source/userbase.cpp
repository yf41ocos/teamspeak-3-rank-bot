#include "userbase.h"

#include "mysql_driver.h"
#include <cppconn/prepared_statement.h>

userbase::userbase(
	const std::string &relay_host,
	const std::string &relay_user,
	const std::string &relay_pwd,
	const std::string &mysql_host,
	const std::string &mysql_port,
	const std::string &mysql_user,
	const std::string &mysql_pwd,
	const std::string &mysql_db,
	const std::string &remote_mysql_host,
	const std::string &remote_mysql_port,
	const std::string &remote_mysql_user,
	const std::string &remote_mysql_pwd,
	const std::string &remote_mysql_db,
	const unsigned long long tickrate
) : relay_host(relay_host),
	relay_user(relay_user),
	relay_pwd(relay_pwd),
	mysql_host(mysql_host),
	mysql_port(mysql_port),
	mysql_user(mysql_user),
	mysql_pwd(mysql_pwd),
	mysql_db(mysql_db),
	remote_mysql_host(remote_mysql_host),
	remote_mysql_port(remote_mysql_port),
	remote_mysql_user(remote_mysql_user),
	remote_mysql_pwd(remote_mysql_pwd),
	remote_mysql_db(remote_mysql_db),
	ts_tickrate(tickrate)
{
	//load stored database
	std::string table_name = "user_online_time";

	sql::mysql::MySQL_Driver *driver;
	driver = sql::mysql::get_driver_instance();
	sql::Connection *con = driver->connect("tcp://"+mysql_host+":"+mysql_port,mysql_user,mysql_pwd);

	sql::Statement *stmt = con->createStatement();
	stmt->execute("USE "+mysql_db);
	stmt->execute("CREATE TABLE IF NOT EXISTS "+table_name+" (unique_id VARCHAR(255) NOT NULL PRIMARY KEY,online_time BIGINT)");

	//fill map, set post counts to 0
	sql::ResultSet *online_time = stmt->executeQuery("SELECT unique_id,online_time FROM "+table_name);

	while(online_time->next()){

		auto &obj = *online_time;

		//every ts user we knew is considered as offline for initialization
		ts_users[obj.getString(1)] = std::make_pair(false,obj.getInt(2));
	}

	delete online_time;
	delete stmt;
	delete con;
}

void userbase::reload_remote_sql_database()
{
	std::string ssh = "sshpass -p "+relay_pwd+" ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no "+relay_user+"@"+relay_host;
	std::string scp = "sshpass -p "+relay_pwd+" scp -o PreferredAuthentications=password -o PubkeyAuthentication=no "+relay_user+"@"+relay_host;

	//dump the two tables
	std::system((ssh+" \"mysqldump --host="+remote_mysql_host+" --user="+remote_mysql_user+" --password="+remote_mysql_pwd+" "+remote_mysql_db+" profile_fields_data > profile_fields_data.sql\"").c_str());
	std::system((ssh+" \"mysqldump --host="+remote_mysql_host+" --user="+remote_mysql_user+" --password="+remote_mysql_pwd+" "+remote_mysql_db+" users > users.sql\"").c_str());

	//copy them
	std::system((scp+":/profile_fields_data.sql ./profile_fields_data.sql").c_str());
	std::system((scp+":/users.sql ./users.sql").c_str());

	//delete them on remote side
	std::system((ssh+" \"rm -f /users.sql && rm -f /profile_fields_data.sql\"").c_str());

	//integrate in local database
	std::system(("mysql --user="+mysql_user+" --password="+mysql_pwd+" "+mysql_db+" < ./users.sql").c_str());
	std::system(("mysql --user="+mysql_user+" --password="+mysql_pwd+" "+mysql_db+" < ./profile_fields_data.sql").c_str());

	sql::mysql::MySQL_Driver *driver;
	driver = sql::mysql::get_driver_instance();
	sql::Connection *con = driver->connect("tcp://"+mysql_host+":"+mysql_port,mysql_user,mysql_pwd);

	sql::Statement *stmt = con->createStatement();
	stmt->execute("USE unitedunicorns_phpbb");
	sql::ResultSet *res = stmt->executeQuery("SELECT pf_teamspeak_id,user_posts FROM ((SELECT user_id,pf_teamspeak_id FROM profile_fields_data WHERE pf_teamspeak_id IS NOT NULL) t1 LEFT JOIN (SELECT user_id,user_posts FROM users WHERE user_posts > 0) t2 ON t1.user_id = t2.user_id)");

	while(res->next()){
		auto &obj = *res;
		std::string ts_uid = obj.getString(1);
		size_t posts = obj.getInt(2);
		forum_users[ts_uid] = posts;
	}

	delete res;
	delete stmt;
	delete con;

#ifdef DEBUG
	for(auto &a : forum_users){
		std::cout << a.first << ": " << a.second << std::endl;
	}
#endif
}

void userbase::rescan_teamspeak(
	const std::string &ts_host,
	const std::string &ts_port,
	const std::string &ts_user,
	const std::string &ts_pwd
)
{
	teamspeak ts(ts_host,ts_port,ts_user,ts_pwd);
	std::vector<std::string> ts_uids;
	ts.get_ids(ts_uids);

	#ifdef DEBUG
		std::cout << "extracted teamspeak uids:" << std::endl;
	#endif
		for(auto &ts_uid : ts_uids){

			auto it = ts_users.find(ts_uid);

			if(it != ts_users.end()){
				//if user exists and was online, count up online time
				if(ts_users[ts_uid].first){
					ts_users[ts_uid].second += ts_tickrate;
				}
				//else set user online status to true
				else {
					ts_users[ts_uid].first = true;
				}
			} else {
				ts_users[ts_uid] = std::make_pair(true,0);
			}


		}
		#ifdef DEBUG
				for(auto &tsuser : ts_users)
				{
					std::cout << tsuser.first << " is " << ((tsuser.second.first) ? "online" : "offline")
					<< " since " << tsuser.second.second << " seconds" << std::endl;
				}
		#endif
}

void userbase::save_user_online_times_to_sql_database()
{
	std::string table_name = "user_online_time";

	sql::mysql::MySQL_Driver *driver;
	driver = sql::mysql::get_driver_instance();
	sql::Connection *con = driver->connect("tcp://"+mysql_host+":"+mysql_port,mysql_user,mysql_pwd);

	sql::Statement *stmt = con->createStatement();
	stmt->execute("USE "+mysql_db);
	sql::PreparedStatement  *prep_stmt = con->prepareStatement("REPLACE INTO "+table_name+"(unique_id,online_time) VALUES(?,?)");

	for(auto &usr : ts_users)
	{
		std::string ts_uid = usr.first;
		unsigned long long ts_online_time = usr.second.second;
		prep_stmt->setString(1,ts_uid);
		prep_stmt->setInt(2,ts_online_time);
		prep_stmt->execute();
	}

	delete prep_stmt;
	delete stmt;
	delete con;
}

void userbase::apply_rules(
	const std::string &ts_host,
	const std::string &ts_port,
	const std::string &ts_user,
	const std::string &ts_pwd)
{
	teamspeak ts(ts_host,ts_port,ts_user,ts_pwd);
	ts.upgrade_memberships(ts_users,forum_users);
}
